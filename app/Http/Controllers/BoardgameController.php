<?php

namespace App\Http\Controllers;

use App\Models\Boardgame;
use Illuminate\Http\Request;

class BoardgameController extends Controller
{
    public function index()
    {
        return Boardgame::all();
    }

    public function store(Request $request)
    {
        $boardgame = new Boardgame();
        $boardgame->title = $request->title;
        $boardgame->type = $request->type;
        $boardgame->value = $request->value;
        $boardgame->save();

        return Boardgame::find($boardgame->id);
    }

    public function show($id)
    {
        return Boardgame::find($id);
    }

    public function update(Request $request, $id)
    {
        $boardgame = Boardgame::find($id);
        $boardgame->title = $request->title;
        $boardgame->type = $request->type;
        $boardgame->value = $request->value;
        
        return Boardgame::find($boardgame->id);
    }

    public function destroy($id)
    {
        Boardgame::find($id)->delete();
        return redirect('/boardgame/list');

    }

    public function editView($id)
    {
        $boardgame = Boardgame::find($id);
        return view('boardgame.edit', ['boardgame' => $boardgame]);
    }

    public function newView()
    {
        return view('boardgame.new');
    }

    public function listView()
    {
        $boardgames = Boardgame::all();
        return view('boardgame.list', ['boardgames' => $boardgames]);
    }

}
