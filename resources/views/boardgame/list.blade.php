<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/style.css">
    <title>Listing Boardgames</title>
</head>
<body>
    <table>
        <thead>
            <th>Title</th>
            <th>Type</th>
            <th>Value</th>
            <th>Modify | Delete</th>
        </thead>
        <tbody>
            @foreach ($boardgames as $boardgame)
            <tr>
                <td>{{ $boardgame->title}}</td>
                <td>{{ $boardgame->type}}</td>
                <td>{{ $boardgame->value}}</td>
                <td><form method="PUT" action="/boardgame/edit/{{$boardgame->id}}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="submit" class="btn btn-danger delete-user" value="MODIFY">
    </form>
                    <form method="POST" action="/api/boardgame/{{$boardgame->id}}">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <input type="submit" class="btn btn-danger delete-user" value="DELETE">
    </form></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @foreach ($boardgames as $boardgame)
    
    @endforeach

</body>
</html>