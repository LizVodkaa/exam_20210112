<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="/api/boardgame" method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="type" placeholder="Type">
        <input type="text" name="value" placeholder="Value">
        <button type="submit">OK</button>
    </form>

</body>
</html>