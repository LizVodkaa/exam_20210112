<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Boardgame DB</title>
</head>
<body>
    <form action="/api/boardgame/{{ $boardgame->id }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <input type="text" name="title" placeholder="Title" value="{{ $boardgame->title }}">
        <input type="text" name="type" placeholder="Type" value="{{ $boardgame->type }}">
        <input type="text" name="value" placeholder="Value" value="{{ $boardgame->value }}">
        <button type="submit">OK</button>
    </form>

</body>
</html>