<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoardgameController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/api/boardgames', [BoardgameController::class, 'index']);
Route::get('/api/boardgame/{id}', [BoardgameController::class, 'show']);
Route::post('/api/boardgame', [BoardgameController::class, 'store']);
Route::put('/api/boardgame/{id}', [BoardgameController::class, 'update']);
Route::delete('/api/boardgame/{id}', [BoardgameController::class, 'destroy']);
Route::get('/boardgame/edit/{id}', [BoardgameController::class, 'editView']);
Route::get('/boardgame/new', [BoardgameController::class, 'newView']);
Route::get('/boardgame/list', [BoardgameController::class, 'listView']);

